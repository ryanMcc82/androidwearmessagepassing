package com.example.ryan.mywearhelloworld;

import java.io.Serializable;

/**
 * Created by Ryan on 8/18/2015.
 */
public class SerialObject implements Serializable {
    private static final long serialVersionUID = 1L;
    String[] accountNames;
    int[] accountBalances;

    public SerialObject(){
        devData();
    }

    public int getNumberOfAccounts(){
        return accountNames.length;
    }

    public String getAccountName(int index){
        String name = accountNames[index];// "*1234"; //TODO

        return name;
    }

    public int getAccountBalance(int index){
        assert (index < accountBalances.length);
        return accountBalances[index];
    }

    private void devData(){
        accountNames = new String[]{"*1231", "*1232", "*1233"};
        accountBalances = new int[]{500, 250, 750};
    }

    public int numberOfAccounts()
    {
        return accountNames.length;
    }
}
