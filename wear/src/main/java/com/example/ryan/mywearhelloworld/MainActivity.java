package com.example.ryan.mywearhelloworld;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.wearable.DataMap;
import com.mariux.teleport.lib.TeleportClient;

import org.apache.commons.lang3.SerializationUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

public class MainActivity extends Activity {

    private TextView mTextView;
    private TeleportClient myTeleport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.text);


        myTeleport = new TeleportClient(this);
        Log.d("My_debug", "OnCreate wear connect");
        //myTeleport.setOnSyncDataItemTask(new SyncDataHandlerApache());
        myTeleport.setOnSyncDataItemTask(new ShowToastOnSyncDataItemTask());
    }

    public void onClick(View v) {
        myTeleport.sendMessage("Hello", null);
    }

        @Override
    protected void onStart() {
        super.onStart();
        myTeleport.connect();
        Log.d("My_debug", "OnStart wear connect");
    }

    @Override
    protected void onStop() {
        super.onStop();
        myTeleport.disconnect();

    }

    public class SyncDataHandlerApache extends TeleportClient.OnSyncDataItemTask{

        @Override
        protected void onPostExecute(DataMap result) {
            Log.d("My_debug", "wear Apache onPostExecute");
            byte[] myBytes = result.getByteArray("data");
            SerialObject passedData = (SerialObject)SerializationUtils.deserialize(myBytes);

            //TextView text = (TextView)findViewById(R.id.text);
            mTextView.setText("Something" + passedData.getAccountName(0));
            Toast.makeText(getApplicationContext(), "Something" + passedData.getAccountName(0), Toast.LENGTH_SHORT).show();

            myTeleport.setOnSyncDataItemTask(new SyncDataHandlerApache());
        }
    }

    public class ShowToastOnSyncDataItemTask extends TeleportClient.OnSyncDataItemTask {

        @Override
        protected void onPostExecute(DataMap dataMap) {

            //let`s get the String from the DataMap, using its identifier key
            String string = dataMap.getString("hello");

            //let`s create a pretty Toast with our string!
            Toast.makeText(getApplicationContext(),string,Toast.LENGTH_SHORT).show();

            myTeleport.setOnSyncDataItemTask(new ShowToastOnSyncDataItemTask());

        }
    }

//    public class SyncDataHandler extends TeleportClient.OnSyncDataItemTask {
//
//        @Override
//        protected void onPostExecute(DataMap dataMap) {
//            byte[] myBytes = dataMap.getByteArray("data");
//            ByteArrayInputStream bis = new ByteArrayInputStream(myBytes);
//            ObjectInput in = null;
//            try {
//                in = new ObjectInputStream(bis);
//                Object o = in.readObject();
//
//            } catch (ClassNotFoundException e) {
//                Log.d("MY_debug", "wear class not found syncing data");
//            } catch (StreamCorruptedException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    bis.close();
//                } catch (IOException ex) {
//                    // ignore close exception
//                }
//                try {
//                    if (in != null) {
//                        in.close();
//                    }
//                } catch (IOException ex) {
//                    // ignore close exception
//                }
//            }
//
//            //let`s get the String from the DataMap, using its identifier key
//            //byte[] myBytes = dataMap.getByteArray("data");
//        }
//    }
}
