package com.example.ryan.mywearhelloworld;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.wearable.DataMap;
import com.mariux.teleport.lib.TeleportClient;

import org.apache.commons.lang3.SerializationUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class MainActivity extends Activity {
    private TeleportClient myTeleport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTeleport = new TeleportClient(this);
        myTeleport.connect();
        Log.d("My_debug", "OnCreate mobile connect");
        myTeleport.setOnGetMessageTask(new ShowToastMessage());
    }

    public void sendDataApatche(View v){
        SerialObject myData = new SerialObject();// TODO Get from server
        byte[] data = SerializationUtils.serialize(myData);
        myTeleport.syncString("hello", "Hello, World!");
        //myTeleport.syncByteArray("data", data);

        Toast.makeText(this,"data sending",Toast.LENGTH_LONG ).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myTeleport.connect();
        Log.d("My_debug", "OnStart mobile connect");
    }

    @Override
    protected void onStop() {
        super.onStop();
        myTeleport.disconnect();
        Log.d("My_debug", "OnStop mobile disconnect");
    }
    //    public void sendData(View v){
//        SerialObject myData = new SerialObject();
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        ObjectOutput out = null;
//        try {
//            out = new ObjectOutputStream(bos);
//            out.writeObject(myData);
//            byte[] myBytes = bos.toByteArray();
//            myTeleport.syncByteArray("data",myBytes);
//        } catch (IOException e) {
//            Log.d("MY_debut", "send exception thrown");
//            e.printStackTrace();
//        }finally {
//            try {
//                if (out != null) {
//                    out.close();
//
//                }
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//            try {
//                bos.close();
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//        }
//
//        //myTeleport.sync
//    }
    private class ShowToastMessage extends TeleportClient.OnGetMessageTask {
        @Override
        protected void onPostExecute(String path) {
            Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show();

            myTeleport.setOnGetMessageTask(new ShowToastMessage());


        }
    }
}
